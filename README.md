# Java项目脚手架

本项目用于初始化Java项目

## 使用方法：

### 初始化
```bash
# 请将<project>替换为项目名，<group>替换为组名
$ git clone git@git.l.jingli365.com:scaffold/java-skeleton.git <project>
# 将原先 origin 改为 skeleton
$ git remote rename origin skeleton
# 添加新的 origin
$ git remote add origin git@git.l.jingli365.com:<group>/<project>.git
$ git push -u origin master
```
### 添加skeleton
从git上直接获取的java项目中，skeleton仓库不存在，需要手动添加

```bash
$ git remote add skelelton git@git.l.jingli365.com:scaffold/java-skeleton.git
$ git fetch skelelton
```

### 更新
```bash
# 请将<project>替换为项目名，<group>替换为组名
$ git fetch skelelton
$ git merge skeleton/master
```

## CHANGLOG

### 2018年8月6日 java-skeleton更新

- gradle/script代码移到gradle-plugins项目，并作为plugin使用
- 通用代码移到 java-libs 项目，并通过maven仓库引用
- 调整代码结构
- 自动引用libs目录下的子目录作为子项目
- 使用dk.jingli365.com/jl-java:v4作为Docker的基础包，包含oracle jdk 8，并配置+8时区

### 2018年5月23日 java-skeleton更新

- 简单变更了web配置包的结构；
- 统一设置了返回的jsonapi的接口结构，详见com.jingli.config.web.json.ResponseJSONEntity类；
- 统一设置了服务器500时异常的返回json，详见com.jingli.config.web.json.ResponseErrorEntity类；
- 新增了sign版jsonapi的返回，返回数据时自动包含sign字段并生成签名;可根据情况使用FastJsonConverter转换器 or FastJsonConverterBySign转换器
